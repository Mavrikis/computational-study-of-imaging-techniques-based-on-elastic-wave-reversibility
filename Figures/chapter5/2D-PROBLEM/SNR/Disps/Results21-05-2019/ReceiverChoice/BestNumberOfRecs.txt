After 31 trials, we have the belowed optimal sets:

2 Receivers: y_3, y_2 --> 4.138396802968701

4 Receivers: y_3, y_2, y_8, x_8 --> 5.7132597924929005

6 receivers: x_8, y_4, y_3, y_2, x_6, x_2 --> 6.326063977222105

8 receivers: x_3, y_4, x_6, y_8, x_4, y_2, y_6, x_2 --> 6.95094008635786

10 receivers: y_2, y_3, y_8, x_8, x_7, y_6, x_4, x_2, y_7, x_6 --> 7.704151834110842
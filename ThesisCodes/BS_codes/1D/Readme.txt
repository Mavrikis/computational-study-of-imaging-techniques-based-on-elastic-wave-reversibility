This code is backward analysis for a 1D beam using beta-Neamark algorithm.

User:

1) Choose a problem to solve. There are two options.
   a) Incident field (case 0)
   b) Scattered field (case 1)
	Here the characteristics of scattered field are (user can change them by default):
		-"path1" of total field recordings from forward step
		-"path2" of incident field recordings from forward step

2) The geometrical and time data of your problem  (will be similar with forward step)

3) The number of imagers and their positions

4) Time reversal process

5) Boundary conditions and beta-Neamark algorithm  (will be similar with forward step)

6) Save results


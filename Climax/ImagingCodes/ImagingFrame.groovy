import climax.contraption
import java.awt.Graphics2D
import java.awt.BasicStroke;
import java.awt.Stroke;

class ImagingFrame implements contraption{
	int id
	Color SelfColor = Color.blue
	Color MotionColor = Color.black
	def xcords=[]
	def ycords=[]
	def val=[]
	def sources=[]
	def receivers=[]
	def scatterers=[]
	def connectivity=[]
	def mode=0 //motion, 0: as diagram, 1: as color distribution
	double dataRangeMax
	double dataRangeMin
	def theGP

	public int getID(){
		return id
	}

	public void SelfPortrait(Graphics2D g2, double ymin, double ymax, int ye, int ys, double xmin, double xmax, int xe, int xs){
		def transX = {double x ->
			return ((int) ((int) ((-xmin * xe + x * (xe - xs) + xmax * xs) / (xmax - xmin))))
		}

		def transY = {double y ->
			return ((int) ((int) ((-ymin * ye + y * (ye - ys) + ymax * ys) / (ymax - ymin))))
		}
		Color origColor=g2.getColor()
		Stroke defaultStroke = g2.getStroke()
		g2.setColor(SelfColor)
		g2.setStroke(new BasicStroke(1))
		
		if(connectivity.isEmpty()){}else{
			connectivity.each{
				int ni=it[0]-1; int nj=it[1]-1
				g2.drawLine(transX(xcords[ni]), transY(ycords[ni]), transX(xcords[nj]),  transY(ycords[nj]));
			}
		}

		float[] fl = new float[1]; fl[0]=9
		Stroke dashed = new BasicStroke(0.5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, fl, 0);
        	g2.setStroke(dashed);

        	g2.setColor(Color.green)
        	if(receivers.isEmpty()){}else{
	        	receivers.each{
	        		double x=it[0]; double y=it[1];
				g2.drawLine(transX(x),ys,transX(x),ye);
				g2.drawLine(xs,transY(y),xe,transY(y));
	        	}
        	}
		fl[0]=8
		dashed = new BasicStroke(1.0, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, fl, 0);
        	g2.setStroke(dashed);
        	g2.setColor(Color.red)
        	if(sources.isEmpty()){}else{
	        	sources.each{
	        		double x=it[0]; double y=it[1];
				g2.drawLine(transX(x),ys,transX(x),ye);
				g2.drawLine(xs,transY(y),xe,transY(y));
	        	}
        	}
        	fl[0]=7
		dashed = new BasicStroke(1.0, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, fl, 0);
        	g2.setStroke(dashed);
        	g2.setColor(Color.gray)
        	if(scatterers.isEmpty()){}else{
	        	scatterers.each{
	        		double x=it[0]; double y=it[1];
				g2.drawLine(transX(x),ys,transX(x),ye);
				g2.drawLine(xs,transY(y),xe,transY(y));
	        	}
        	}
		g2.setColor(origColor)
		g2.setStroke(defaultStroke)
	}
	
	public void Motion(Graphics2D g2, double ymin, double ymax, int ye, int ys, double xmin, double xmax, int xe, int xs, int step, double scale){
		switch(mode){
			case 0:
			def transX = {double x ->
				return ((int) ((int) ((-xmin * xe + x * (xe - xs) + xmax * xs) / (xmax - xmin))))
			}
	
			def transY = {double y ->
				return ((int) ((int) ((-ymin * ye + y * (ye - ys) + ymax * ys) / (ymax - ymin))))
			}
	
			Color origColor=g2.getColor()
			g2.setColor(MotionColor)
			if(connectivity.isEmpty()){}else{connectivity.each{
					int n1=it[0]-1; int n2=it[1]-1
					double[] normal=getNormal(it)
					normal[0]=-normal[0];normal[1]=-normal[1];normal[2]=-normal[2]
		          	int xp1 = transX(normal[0]*scale*val[n1][step]+xcords[n1]);
					int yp1 = transY(normal[1]*scale*val[n1][step]+ycords[n1]);
					int xp2 = transX(normal[0]*scale*val[n2][step]+xcords[n2]);
					int yp2 = transY(normal[1]*scale*val[n2][step]+ycords[n2]);
					g2.drawLine(xp1, yp1, xp2, yp2)
				}
			}
			
			g2.setColor(origColor)
			break;
			case 1:
			theGP.dataRangeMax=dataRangeMax
			theGP.dataRangeMin=dataRangeMin
			theGP.colorCount=64
			theGP.colorArray = new Color[theGP.colorCount];
        		theGP.setupColors();
			def transX = {double x ->
				return ((int) ((int) ((-xmin * xe + x * (xe - xs) + xmax * xs) / (xmax - xmin))))
			}
	
			def transY = {double y ->
				return ((int) ((int) ((-ymin * ye + y * (ye - ys) + ymax * ys) / (ymax - ymin))))
			}
		
			Color origColor=g2.getColor()
			Stroke defaultStroke = g2.getStroke()
			g2.setStroke(new BasicStroke(7))
			
			if(connectivity.isEmpty()){}else{
				connectivity.each{
					int n1=it[0]-1; int n2=it[1]-1
					double dataValue=(val[n1][step]+val[n2][step])/2.0
					double colorLocation =((theGP.colorCount-1.0)*(dataValue - dataRangeMin))/(dataRangeMax - dataRangeMin);
					int colorIndex = (int)colorLocation;
					if(colorIndex >= theGP.colorCount) colorIndex = theGP.colorCount-1;
					if(colorIndex < 0) colorIndex = 0;
					g2.setColor(theGP.colorArray[colorIndex]);
					g2.drawLine(transX(xcords[n1]), transY(ycords[n1]), transX(xcords[n2]),  transY(ycords[n2]));
				}
			}
			g2.setColor(origColor)
			g2.setStroke(defaultStroke)
			break;
			default:
			println("Motion not yet defined for mode:"+mode)
		}
	}
	
	public double maximum_X_coordinate(){if(xcords.isEmpty()){0.0}else{xcords.max()}}
	public double minimum_X_coordinate(){if(xcords.isEmpty()){0.0}else{xcords.min()}}
	public double maximum_Y_coordinate(){if(ycords.isEmpty()){0.0}else{ycords.max()}}
	public double minimum_Y_coordinate(){if(ycords.isEmpty()){0.0}else{ycords.min()}}

	def addCoord={x,y->
		xcords.add(x as double) 
		ycords.add(y as double)
	}

	def addVal={
		val.add(it) 
	}

	def addSource={
		sources.add(it)
	}

	def addReceiver={
		receivers.add(it)
	}

	def addConnection={
		connectivity.add(it)
	}

	def addScatterer={
		scatterers.add(it)
	}
		
	def getNormal={
		double[] normal = new double[3];
		double[] vxsi = new double[3];
		double[] veta = new double[3];
		(0..2).each{normal[it]=0.0;vxsi[it]=0.0;veta[it]=0.0;}
		vxsi[0]=xcords[it[1]-1] -xcords[it[0]-1];
		vxsi[1]=ycords[it[1]-1] -ycords[it[0]-1];
		vxsi[2]=0.0;
		veta[0]=0.0;
		veta[1]=0.0;
		veta[2]=1.0;
		normal[0]=vxsi[1]*veta[2]-vxsi[2]*veta[1];
		normal[1]=vxsi[2]*veta[0]-vxsi[0]*veta[2];
		normal[2]=vxsi[0]*veta[1]-vxsi[1]*veta[0];
		double n=Math.sqrt(
		      normal[0]*normal[0]+
		      normal[1]*normal[1]+
		      normal[2]*normal[2]
		      );
		normal[0]/=n;
		normal[1]/=n;
		normal[2]/=n;
		return normal;
	}

	def findLimitVals(){
		dataRangeMax=Double.NEGATIVE_INFINITY
		dataRangeMin=Double.POSITIVE_INFINITY
		val.each{
			it.each{
				if(dataRangeMax<it)dataRangeMax=it
				if(dataRangeMin>it)dataRangeMin=it
			}
		}
		theGP.dataRangeMax=dataRangeMax
		theGP.dataRangeMin=dataRangeMin
	}
}


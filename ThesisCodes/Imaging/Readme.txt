These imaging codes are for 1D beam and 2D frame structure 

User:

1) Run the "Imaging1D2DFrame.groovy" file

2) 1D Beam ("ImagingBeam.climax") & 2D Frame structure ("ImagingFrame.climax")
	
a) Think the problem which wants to study and choose the 			
corresponding path.
b) Choose the Degree of Freedom (DoF) and the position (i.e DoF_id)  	
of some number of stimulators which used in Backward step.
c) If noise considered, please make the boolean variable "noise" as 	
true and give a pathN.  
d) Choose an imaging variable and then the exact discrete time step 
of imaging ("ex_time"). 

... some proceedings of data
	
e) The Graphical Panel settings

----------------------------------------------------------------------------------

Note 1: The file "GeomFileImagers.txt" is the format which the imaging codes can read the node's coordinators and the connections between them.
The .txt files both for 1D and 2D cases are respectively created for thesis problems and could change.	

Note 2: The file "Receivers.txt" in 1D case helps to locate the receivers of thesis problems on the Graphical Panel.	


		
Instructions to use Imaging Codes


Step 1: Open the file with name "ImagingFrame.groovy".

Step 2: Open the file with name "imaging_1_pchr_as_Im_Fr_upd3.climax".

On "imaging_1_pchr_as_Im_Fr_upd3.climax" code,

Step 3: Read the comments in the block with paths and define the right path. There are correct paths from Server and External Hard Disk.

Step 4: Define to "receivers" array the DoF and the receiver you want to image.

Step 5: In the block with name "Imaging Time", define the right descrite time step correspondingly with your option on Step 3.

Step 6: In the block with name "Locations", if you study scattered field, uncomment the lines which concern scattered field otherwise comment them.

Step 7: Depend on the problem you choose to study on Step 3, comment or uncomment the corresponding lines in the loop "(0..<imagers_list.size()).each{....}". For example, if you want to image axial DoF of displacements without Gaussian Noise, uncomment "lines_r1= ..." and "r1.add(...)".  

Step 8: Choose whatever you want to image with the command "val2video.add(...)". For the example on Step 7, choose "val2video.add(r1)".

Step 9: Firstly, run the code with name "ImagingFrame.groovy". 

Step 10: Then, run the code with name "imaging_1_pchr_as_Im_Fr_upd3.climax".



 
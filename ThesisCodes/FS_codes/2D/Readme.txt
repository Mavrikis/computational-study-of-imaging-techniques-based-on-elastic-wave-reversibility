This code is a dynamic forward analysis for a 2D frame structure using beta-Neamark algorithm.

User:

1) Choose a problem to solve. There are two options.
   a) Incident field (case 0)
   b) Total field (case 1)
	Here the characteristics of total field are (user can change them by default):
		-Location of damage between 7 and 8 main nodes of structure
		-Modulus of elasticity equals to 0.2 Pa

2) Select the path which user should save the results in .txt files

3) Select the geometrical and time data of your problem

4) Select the number of receivers and their positions

5) Select the excitation/source type, position and DoF/DoFs

6) Select boundary conditions and beta-Neamark algorithm

7) Save results
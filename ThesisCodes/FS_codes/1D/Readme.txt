This code is a dynamic forward analysis for a 1D beam using beta-Neamark algorithm.

User:

1) Choose a problem to solve. There are two options.
   a) Incident field (case 0)
   b) Total field (case 1)
	Here the characteristics of total field are (user can change them by default):
		-Location of damage between (0.1795*L, 0.0) and (0.18*L, 0.0) 
		-Modulus of elasticity equals to 0.2 Pa

2) Select the path which user should save the results in .txt files

3) Select the geometrical and time data of your problem

4) Select the number of receivers and their positions

5) Select the excitation/source type, position and DoF/DoFs

6) Select boundary conditions and beta-Neamark algorithm

7) Save results